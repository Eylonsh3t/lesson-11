#include "threads.h"

void I_Love_Threads()
{
	std::cout << "I Love Threads" << std::endl;
}

void call_I_Love_Threads()
{
	std::thread t1(I_Love_Threads);
	t1.join();
}

void printVector(std::vector<int> primes)
{
	for (int i = 0; i < primes.size(); i++)
	{
		std::cout << primes.at(i) << std::endl;
	}
}

void getPrimes(int begin, int end, std::vector<int>& primes)
{
	int i, m = 0;
	do
	{
		int flag = 0;
		m = begin / 2;
		for (i = 2; i <= m; i++)
		{
			if (begin % i == 0)
			{
				flag = 1;
				break;
			}
		}
		if (flag == 0)
			primes.push_back(begin);

		begin++;
	} while (begin < end);
}

std::vector<int> callGetPrimes(int begin, int end)
{
	clock_t t;
	std::vector<int> primes;
	t = clock();
	std::thread t1(getPrimes, std::ref(begin), std::ref(end), std::ref(primes));
	t1.join();
	t = clock() - t;
	std::cout << "It took " << (((float)t) / CLOCKS_PER_SEC) << " seconds." << std::endl;
	return primes;
}

void writePrimesToFile(int begin, int end, std::ofstream& file)
{
	int i, m = 0;
	do
	{
		int flag = 0;
		m = begin / 2;
		for (i = 2; i <= m; i++)
		{
			if (begin % i == 0)
			{
				flag = 1;
				break;
			}
		}
		if (flag == 0)
			file << begin << std::endl;

		begin++;
	} while (begin < end);
}

void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N)
{
	std::ofstream myFile;
	myFile.open(filePath);
	int param1 = end / N;
	std::thread* t = new std::thread[N];
	int range = (end - begin) / N;
	for (int i = 0; i < N; i++)
	{
		if (i == 0)
		{
			t[0] = std::thread(writePrimesToFile, std::ref(begin), std::ref(range), std::ref(myFile));
		}
		else
		{
			int beginRange = range * i;
			int endRange = range * (i + 1);
			t[i] = std::thread(writePrimesToFile, std::ref(beginRange), std::ref(endRange), std::ref(myFile));
		}
		t[i].join();
	}
	delete[] t;
	myFile.close();
}
